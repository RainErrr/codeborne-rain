package codeborne.BasicWebServer;

import java.nio.charset.StandardCharsets;
import java.util.Date;

abstract class Response {
    String statusText;
    String contentType;

     public Response(String statusText, String contentType) {
        this.statusText = statusText;
        this.contentType = contentType;
    }

    abstract byte[] body();

    abstract int bodyLength();

    byte[] header(){
        return ("HTTP/1.1 " + statusText + "\r\n" +
                "Server: Java HTTP Server from Rain 1.0" + "\r\n" +
                "Date: " + new Date() + "\r\n" +
                "Content-Type: " + contentType + "\r\n" +
                "Content-length: " + bodyLength() + "\r\n\r\n").getBytes(StandardCharsets.UTF_8);
    }

    public final byte[] responseAsByteArray() {
        byte[] headAndBody = new byte[header().length + body().length];
        System.arraycopy(header(), 0, headAndBody, 0, header().length);
        System.arraycopy(body(), 0, headAndBody, header().length, body().length);
        return headAndBody;
    }
}
