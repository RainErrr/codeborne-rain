package codeborne.BasicWebServer;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public void webServer() {
        try {
            ServerSocket server = new ServerSocket(8888);
            System.out.println("Server started, listening on port 8888");

            while (true) {
                try (Socket session = server.accept();
                     InputStream in = session.getInputStream();
                     BufferedOutputStream out = new BufferedOutputStream(session.getOutputStream())) {
                    System.out.println("session start!");
                    RequestHandler request = new RequestHandler();
                    ResponseHandler response = new ResponseHandler();
                    Request fullRequest = request.returnRequestWithUpdatedBody(in, request.requestInObject(in));
                    System.out.println(fullRequest);
                    byte[] fullResponse = response.finalResponse(fullRequest);
                    out.write(fullResponse);
                    out.flush();
                    System.out.println("response sent!");
                    System.out.println("session end!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
