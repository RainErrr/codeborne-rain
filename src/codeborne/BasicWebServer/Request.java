package codeborne.BasicWebServer;

import java.util.Arrays;
import java.util.Map;

public class Request {
    final private String methodType;
    final private String filePath;
    final private String httpVersion;
    final private boolean doesBodyExist;
    final private int bodyLength;
    final private Map<String, String> requestHeaders;
    private byte[] body;

    public Request(String methodType, String filePath, String httpVersion, boolean doesBodyExist, int bodyLength,
                   Map<String, String> requestHeaders, byte[] body) {
        this.methodType = methodType;
        this.filePath = filePath;
        this.httpVersion = httpVersion;
        this.doesBodyExist = doesBodyExist;
        this.bodyLength = bodyLength;
        this.requestHeaders = requestHeaders;
        this.body = body;
    }

    String getFilePath() {
        return filePath;
    }

    String getMethodType() {
        return methodType;
    }

    boolean isDoesBodyExist() {
        return doesBodyExist;
    }

    int getBodyLength() {
        return bodyLength;
    }

    byte[] getBody() {
        return body;
    }

    void setBody(byte[] body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Request{" +
                "methodType='" + methodType + '\'' +
                ", filePath='" + filePath + '\'' +
                ", httpVersion='" + httpVersion + '\'' +
                ", doesBodyExist=" + doesBodyExist +
                ", bodyLength=" + bodyLength +
                ", requestHeaders=" + requestHeaders +
                ", body=" + Arrays.toString(body) +
                '}';
    }
}