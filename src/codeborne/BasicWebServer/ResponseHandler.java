package codeborne.BasicWebServer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;

public class ResponseHandler {

    private static final String WEB_ROOT = "Resources";
    private static final String HOME = "index.html";
    private static final String NOT_IMPLEMENTED = "501.html";
    private static final String FILE_NOT_FOUND = "404.html";

    byte[] finalResponse(Request request) throws IOException {
        return responseHandler(request).responseAsByteArray();
    }

    private Response responseHandler(Request request) throws IOException {
        switch (request.getMethodType()) {
            case "GET":
                return new GetResponse(
                        responseStatusText(request),
                        responseContentType(request),
                        body(request));
            case "POST":
                return new PostResponse(
                        responseStatusText(request),
                        responseContentType(request),
                        body(request));
            case "HEAD":
               return new HeadResponse(
                       responseStatusText(request),
                       responseContentType(request),
                       body(request));
            default:
                return new NotImplemented501Response("501 Not implemented",
                        "text/html",
                        readFileForResponse(new File(WEB_ROOT, NOT_IMPLEMENTED)));
        }
    }

    String responseStatusText(Request request) {
        return statusText(createResponseFile(filePathFixForEdgeCases
                (turnFileNameToFilePath(parsedFileName(request.getFilePath())))));
    }

    String responseContentType(Request request) {
        return contentType(filePathFixForEdgeCases
                (turnFileNameToFilePath(parsedFileName(request.getFilePath()))));
    }

    byte[] body(Request request) throws IOException {
        return readFileForResponse(createResponseFile(filePathFixForEdgeCases
                (turnFileNameToFilePath(parsedFileName(request.getFilePath())))));
    }

    String parsedFileName(String requestedFileName) {
        return requestedFileName.equals("/") || requestedFileName.isEmpty() ? HOME :
                requestedFileName.replace("/", "");
    }

    Path turnFileNameToFilePath(String parsedFileName) {
        return parsedFileName.isEmpty() ? Path.of(WEB_ROOT, HOME) :
                Path.of(WEB_ROOT, parsedFileName);
    }

    String filePathFixForEdgeCases(Path filePath) {
        String newFilePath;
        if (filePath.toFile().exists()) newFilePath = filePath.toFile().getName();
        else newFilePath = FILE_NOT_FOUND;
        return newFilePath;
    }

    File createResponseFile(String fixedFilePath) {
        return new File(WEB_ROOT, fixedFilePath);
    }

    byte[] readFileForResponse(File responseFile) throws IOException {
        if (!responseFile.exists()) responseFile = new File(WEB_ROOT, FILE_NOT_FOUND);
        byte[] readFileInByteArray;
        try (FileInputStream readFile = new FileInputStream(responseFile)) {
            readFileInByteArray = new byte[readFile.available()];
            readFile.readNBytes(readFileInByteArray, 0 , readFileInByteArray.length);
        }
        return readFileInByteArray;
    }

    String statusText(File responseFile) {
        if (responseFile.exists() && !responseFile.getName().equals(FILE_NOT_FOUND)
                && !responseFile.getName().equals(NOT_IMPLEMENTED)) return "200 OK";
        else if (responseFile.getName().equals(FILE_NOT_FOUND)) return "404 File Not Found";
        else return "501 Not Implemented";
    }

    String contentType(String fixedFilePath) {
        if (fixedFilePath.endsWith(".html")) return "text/html";
        else if (fixedFilePath.endsWith(".ico")) return "image/icon";
        else if (fixedFilePath.endsWith(".png")) return "image/png";
        else if (fixedFilePath.endsWith(".jpg")) return "image/jpeg";
        else return "text/plain";
    }
}
