package codeborne.BasicWebServer;

public class GetResponse extends Response {
    private final byte[] content;

    public GetResponse(String status, String type, byte[] content) {
        super(status, type);
        this.content = content;
    }

    @Override
    byte[] body() {
        return content;
    }

    @Override
    int bodyLength() {
        return content.length;
    }
}
