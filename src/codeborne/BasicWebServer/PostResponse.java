package codeborne.BasicWebServer;

public class PostResponse extends Response{
    private final byte[] content;

    public PostResponse(String status, String type, byte[] content) {
        super(status, type);
        this.content = content;
    }

    @Override
    byte[] body() {
        return content;
    }

    @Override
    int bodyLength() {
        return content.length;
    }
}
