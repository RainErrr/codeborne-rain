package codeborne.BasicWebServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RequestHandler {

    String requestHeaderFromStream(InputStream inputStream) throws IOException {
        StringBuilder requestHeader = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.isBlank()) {
                requestHeader.append(line);
                break;
            }
            requestHeader.append(line).append("\r\n");
        }
        return requestHeader.toString();
    }

    String[] splitRequestHeader(String requestHeaderFromStream) {
        return requestHeaderFromStream.split("\r\n");
    }

    Map<String, String> requestHeaderToMap(String[] splitRequestHeader) {
        String[] copyOfInitialArray = splitRequestHeader.clone();
        Map<String, String> headersMap = new HashMap<>();
        for (int i = 1; i < copyOfInitialArray.length; i++) {
            String[] temp = Arrays.stream(copyOfInitialArray[i].split(" ")).
                    filter(s -> s.length() > 0).toArray(String[]::new);
            headersMap.put(temp[0].isBlank() ? "" : temp[0], temp[1].isBlank() ? "" : temp[1]);
        }
        return headersMap;
    }

    boolean findIfRequestHasBody(String[] splitRequestHeader) {
        return Arrays.stream(splitRequestHeader).anyMatch(s -> s.startsWith("Content-Length: "));
    }

    String parseOutLineWithLengthOfBody(String[] splitRequestHeader) {
        return Arrays.stream(splitRequestHeader).filter(s -> s.startsWith("Content-Length: ")).
                findFirst().orElse("");
    }

    int lengthOfBody(String parseOutLineWithLengthOfBody) {
        return parseOutLineWithLengthOfBody.length() <= 16 ||
                parseOutLineWithLengthOfBody.substring(16).trim().isEmpty() ? 0
                : Integer.parseInt(parseOutLineWithLengthOfBody.substring(16));
    }

    Request requestInObject(InputStream inputStream) throws IOException {
        String requestHeader = requestHeaderFromStream(inputStream);
        String[] statusLine = splitStatusLine(headerStatusLine(splitRequestHeader(requestHeader)));
        boolean doesBodyExist = findIfRequestHasBody(splitRequestHeader(requestHeader));
        int lengthOfBody = doesBodyExist ?
                lengthOfBody(parseOutLineWithLengthOfBody(splitRequestHeader(requestHeader))) : 0;
        Map<String, String> headersMap = requestHeaderToMap(splitRequestHeader(requestHeader));
        return new Request(
                requestMethod(statusLine),
                requestedFile(statusLine),
                requestHttpVersion(statusLine),
                doesBodyExist,
                lengthOfBody,
                headersMap,
                new byte[0]
        );
    }

    Request returnRequestWithUpdatedBody(InputStream inputStream, Request requestInObject) throws IOException {
        if (requestInObject.isDoesBodyExist()) {
            requestInObject.setBody(inputStream.readNBytes(requestInObject.getBodyLength()));
        }
        return requestInObject;
    }

    String headerStatusLine(String[] splitRequestHeader) {
        return splitRequestHeader.length < 1 ? "" : splitRequestHeader[0];
    }

    String[] splitStatusLine(String headerStatusLine) {
        return headerStatusLine.length() < 1 ?
                new String[]{} :
                Arrays.stream(headerStatusLine.split(" ")).
                        filter(s -> s.length() > 0).toArray(String[]::new);
    }

    String requestMethod(String[] splitStatusLine) {
        return splitStatusLine.length < 1 ? "" : splitStatusLine[0];
    }

    String requestedFile(String[] splitStatusLine) {
        return splitStatusLine.length < 2 ? "" : splitStatusLine[1];
    }

    String requestHttpVersion(String[] splitStatusLine) {
        return splitStatusLine.length < 3 ? "" : splitStatusLine[2];
    }
}
