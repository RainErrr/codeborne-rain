package codeborne.BasicWebServer;

public class NotImplemented501Response extends Response{
    private final byte[] content;

    public NotImplemented501Response(String status, String type, byte[] content) {
        super(status, type);
        this.content = content;
    }

    @Override
    byte[] body() {
        return content;
    }

    @Override
    int bodyLength() {
        return content.length;
    }
}
