package codeborne.BasicWebServer;

public class HeadResponse extends Response{
    private final byte[] content;

    public HeadResponse(String status, String type, byte[] content) {
        super(status, type);
        this.content = content;
    }

    @Override
    byte[] body() {
        return new byte[0];
    }

    @Override
    int bodyLength() {
        return content.length;
    }
}
