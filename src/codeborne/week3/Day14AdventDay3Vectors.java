package codeborne.week3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static codeborne.week1.Day03NumbersReader.fileReaderListStr;
import static java.lang.Math.*;
import static java.lang.Math.abs;

public class Day14AdventDay3Vectors {

    public static void main(String[] args) throws IOException {
        Day14AdventDay3Vectors advent = new Day14AdventDay3Vectors();

        System.out.println(advent.distance(advent.intersectionPointsOfVectors
                (
                        advent.vectorToList(
                                fileReaderListStr("advent03first.txt")),
                        advent.vectorToList(
                                fileReaderListStr("advent03Second.txt")
                        )
                )));
        System.out.println(advent.steps(advent.intersectionPointsOfVectors
                (
                        advent.vectorToList(
                                fileReaderListStr("advent03first.txt")),
                        advent.vectorToList(
                                fileReaderListStr("advent03Second.txt")
                        )
                )
        ));
    }

    public int distance(List<Intersections> intersections) {
        List<Integer> distances = new ArrayList<>();
        for (Intersections i : intersections) {
            if (i.getxIntersect() != 0 && i.getyIntersect() != 0) {
                distances.add(abs(i.getxIntersect()) + abs(i.getyIntersect()));
            }
        }
        return distances.stream().min(Integer::compare).get();
    }

    public int steps(List<Intersections> xPoints) {
        List<Integer> steps = new ArrayList<>();
        for (Intersections i : xPoints) {
            steps.add(i.getSteps());
        }
        return steps.stream().min(Integer::compare).get();
    }

    private int manhattanDistance(Line v1) {
        return abs(v1.getxStart() - v1.getxEnd()) + abs(v1.getyStart() - v1.getyEnd());
    }

    private int extraStepsToReachXPoint(Line v1, Intersections i1) {
        return abs(v1.getxStart() - i1.getxIntersect()) + abs(v1.getyStart() - i1.getyIntersect());
    }

    private boolean checkIfLinesCross(Line line) {
        return line.getxStart() == line.getxEnd();
    }

    private Intersections calculateXPoints(Line line, Line line2, Line vertical, Line horizontal, int steps1, int steps2) {
        Intersections placeHolder = new Intersections(vertical.getxStart(),
                        horizontal.getyStart(), steps1 + steps2);
        int placeHolderSteps = placeHolder.getSteps() +
                (extraStepsToReachXPoint(line, placeHolder) +
                        extraStepsToReachXPoint(line2, placeHolder));
        placeHolder.setSteps(placeHolderSteps);
        return placeHolder;
    }

    private List<Intersections> intersectionPointsOfVectors(List<Line> wireOne, List<Line> wireTwo) {
        List<Intersections> intersections = new ArrayList<>();
        int steps1 = 0;
        for (Line line : wireOne) {
            int steps2 = 0;
            for (Line line2 : wireTwo) {
                if (checkIfLinesCross(line) ^ checkIfLinesCross(line2)) {
                    Line vertical = checkIfLinesCross(line) ? line : line2;
                    Line horizontal = checkIfLinesCross(line) ? line2 : line;

                    int minX = min(horizontal.getxStart(), horizontal.getxEnd());
                    int minY = min(vertical.getyStart(), vertical.getyEnd());

                    int maxX = max(horizontal.getxStart(), horizontal.getxEnd());
                    int maxY = max(vertical.getyStart(), vertical.getyEnd());

                    if (vertical.getxStart() >= minX && vertical.getxStart() <= maxX
                            && horizontal.getyStart() >= minY && horizontal.getyEnd() <= maxY) {
                        intersections.add(calculateXPoints(line, line2, vertical, horizontal, steps1, steps2));
                    }
                }
                steps2 += manhattanDistance(line2);
            }
            steps1 += manhattanDistance(line);
        }
        return intersections;
    }

    private List<Line> vectorToList(List<String> firstList) {
        List<Line> wireList = new ArrayList<>();
        int xStart = 0, yStart = 0;
        for (String s : firstList) {
            int[] pointer = rightDownLeftUpPointer(String.valueOf(s.charAt(0)));
            int coordinate = Integer.parseInt(s.substring(1));
            int xEnd = xStart + (coordinate * pointer[0]);
            int yEnd = yStart + (coordinate * pointer[1]);
            wireList.add(new Line(xStart, xEnd, yStart, yEnd));
            xStart = xEnd;
            yStart = yEnd;
        }
        return wireList;
    }

    private int[] rightDownLeftUpPointer(String inputLetter) {
        int[] pointer;
        switch (inputLetter) {
            case "R":
                pointer = new int[]{1, 0};
                break;
            case "D":
                pointer = new int[]{0, -1};
                break;
            case "L":
                pointer = new int[]{-1, 0};
                break;
            case "U":
                pointer = new int[]{0, 1};
                break;
            default:
                return new int[]{0, 0, 7};
        }
        return pointer;
    }
}
