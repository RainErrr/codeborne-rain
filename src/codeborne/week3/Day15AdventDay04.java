package codeborne.week3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day15AdventDay04 {
    public static void main(String[] args) {
        Day15AdventDay04 advent = new Day15AdventDay04();
        System.out.println(advent.numberRange(168630, 718098));
    }

    private int whichMeetCriteria(List<Integer> numbersRange) {
        List<Integer> numbersThatMeetCriteria = new ArrayList<>();
        for (Integer i : numbersRange) {
            if (digitsAscendingOrder(i) && twoAdjacentDigitsSame(i) && atLeastOnePair(i)) {
                numbersThatMeetCriteria.add(i);
            }
        }
        return numbersThatMeetCriteria.size();
    }

    private boolean atLeastOnePair(Integer number) {
        Map<Integer, Integer> digitsOccurrence = new HashMap<>();
        for (int i = 0; i < number.toString().length(); i++) {
            int digit = Integer.parseInt(String.valueOf(number.toString().charAt(i)));
            if (digitsOccurrence.containsKey(digit)) {
                digitsOccurrence.put(digit, digitsOccurrence.get(digit) + 1);
            }
            digitsOccurrence.putIfAbsent(digit, 1);
        }
        return digitsOccurrence.containsValue(2);
    }

    private boolean digitsAscendingOrder(Integer number) {
        boolean isOrNot = false;
        for (int i = 0; i < number.toString().length(); i++) {
            for (int j = i; j < number.toString().length(); j++) {
                if (number.toString().charAt(i) <= number.toString().charAt(j)) {
                    isOrNot = true;
                } else {
                    return false;
                }
            }
        }
        return isOrNot;
    }

    private boolean twoAdjacentDigitsSame(Integer number) {
        boolean isOrNot = false;
        for (int i = 0; i < number.toString().length() - 1; i++) {
            if (number.toString().charAt(i) == number.toString().charAt(i + 1)) {
                isOrNot = true;
                break;
            }
        }
        return isOrNot;
    }

    public int numberRange(int start, int end) {
        List<Integer> numbersRange = new ArrayList<>();
        if (start < end && start >= 99999 && end <= 999999) {
            for (int i = start; i < end + 1; i++) {
                numbersRange.add(i);
            }
        } else {
            return 0;
        }
        return whichMeetCriteria(numbersRange);
    }
}
