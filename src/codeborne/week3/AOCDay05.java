package codeborne.week3;

import codeborne.week1.Day03NumbersReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AOCDay05 {
    private Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        AOCDay05 advent = new AOCDay05();
        System.out.println(advent.programToList(Day03NumbersReader.fileReaderListInt("aocDay05.txt")));
    }

    public List<Integer> programToList(List<Integer> program) {
        List<Integer> copyOfProgram = new ArrayList<>(program);
        System.out.println("Enter a number to run the Program: ");
        String userInput = scanner.nextLine();
        copyOfProgram.set(copyOfProgram.get(1), Integer.parseInt(userInput));
        return copyOfProgram;
    }

    private List<Integer> diagnosticProgram(List<Integer> program) {
        List<Integer> copyOfProgram = new ArrayList<>(program);
        int pointerMovement = 0;
        for (int i = 0; i < copyOfProgram.size(); i = i + pointerMovement) {
            int firstVariable = copyOfProgram.get(copyOfProgram.get(i + 1));
            int secondVariable = copyOfProgram.get(copyOfProgram.get(i + 2));
            if (copyOfProgram.get(i) == 99) {
                return copyOfProgram;
            } else if (copyOfProgram.get(i) == 1) {
                copyOfProgram.set(copyOfProgram.get(i + 3), firstVariable + secondVariable);
                pointerMovement = 4;
            } else if(copyOfProgram.get(i) == 2) {
                copyOfProgram.set(copyOfProgram.get(i + 3), firstVariable * secondVariable);
                pointerMovement = 4;
            } else if(copyOfProgram.get(i) == 3) {
                System.out.println("Sisesta number");
                String userInput = scanner.nextLine();
                pointerMovement = 2;
            } else if(copyOfProgram.get(i) == 4) {
                System.out.println(copyOfProgram.get(copyOfProgram.get(i + 1)));
                pointerMovement = 2;
            }
        }
        return copyOfProgram;
    }

    private int numberToInstructions(int number) {
        String variable = String.valueOf(number);
        int newInstruction = 0;
        if (variable.length() > 2) {
            newInstruction = Integer.parseInt(variable.substring(variable.length()-2));
        }
        return newInstruction;
    }

    private void modeConverter(int number, List<Integer> program) {
        String variable = String.valueOf(number).substring(0, String.valueOf(number).length()-2);
        if (String.valueOf(number).length() > 2) {
            for (int i = 0; i < variable.length(); i++) {
                if (variable.charAt(i) == 0) {

                }
            }
        }
    }
}
