package codeborne.week1;

import java.util.ArrayList;
import java.util.List;

public class Day01FibonacciSequence {

    public static List<Integer> fibonacciSequence(int n) {
        List<Integer> sequenceList = new ArrayList<>();
        if (n < 0) {
            System.out.println("Input must be bigger than Zero");
            return  sequenceList;
        }
        int number1 = 0, number2 = 1, number3;
        sequenceList.add(number1);
        if (n >= 2) {
            sequenceList.add(number2);
        }
        for (int i = 2; i < n; i++) {
            number3 = number1 + number2;
            number1 = number2;
            number2 = number3;
            sequenceList.add(number3);
        }
        return sequenceList;
    }
}
