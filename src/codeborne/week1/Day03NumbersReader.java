package codeborne.week1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Day03NumbersReader {

    public static List<Integer> fileReaderListInt(String inputFileName) throws IOException {
        List<String> listOfStrings = Files.readAllLines(Paths.get("Resources",  inputFileName));
        List<Integer> listOfIntegers = new ArrayList<>();
        for (String string : listOfStrings) {
            String[] arrayOfStrings = string.split(",");
            for (String s : arrayOfStrings) {
                listOfIntegers.add(Integer.valueOf(s));
            }
        }
        return listOfIntegers;
    }

    public static List<String> fileReaderListStr(String inputFileName) throws IOException {
        List<String> listOfStrings = Files.readAllLines(Paths.get("Resources",inputFileName));
        List<String> newListOfStrings = new ArrayList<>();
        for (String string: listOfStrings) {
            String[] arrayString = string.split(",");
            Collections.addAll(newListOfStrings, arrayString);
        }
        return newListOfStrings;
    }
}
