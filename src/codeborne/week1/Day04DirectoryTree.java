package codeborne.week1;

import java.io.File;
import java.util.Arrays;

public class Day04DirectoryTree {


    public static void main(String[] args) {
        File[] parentDirectory = new File(".").listFiles();
        assert parentDirectory != null;
        System.out.println(recursivePrinter(0, parentDirectory));
    }

    public static void directorySorter(File[] files) {
        Arrays.sort(files, (file1, file2) -> {
            if (file1.isDirectory() && !file2.isDirectory()) {
                return -1;
            } else if (!file1.isDirectory() && file2.isDirectory()) {
                return 1;
            } else {
                return file1.compareTo(file2);
            }
        });
    }

    public static StringBuilder recursivePrinter(int indent, File[] files) {
        StringBuilder directoryTree = new StringBuilder();
        directorySorter(files);
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile() && !files[i].isHidden()) {
                boolean isLast = files.length -1 == i;
                    indentForFiles(files[i], directoryTree, indent, isLast);
            }
            if (files[i].isDirectory() && !files[i].isHidden()) {
                indentForFolders(files[i], directoryTree, indent);
                File[] filesDeep = files[i].listFiles();
                directorySorter(filesDeep);
                directoryTree.append(recursivePrinter(indent + 1, filesDeep));
            }
        }
        return directoryTree;
    }
    public static void indentForFiles(File files, StringBuilder sb, int indent, boolean islast) {
            sb.append("\t".repeat(indent))
                    .append((indent == 0) ? "": (!islast) ? "|-" :"`-")
                    .append(files.getName())
                    .append("\n");
    }

    public  static void indentForFolders(File file, StringBuilder sb, int indent) {
            sb.append("\t".repeat(indent))
                    .append(indent > 0 ? "`---" : "")
                    .append(file.getName())
                    .append("\n");
    }
}
