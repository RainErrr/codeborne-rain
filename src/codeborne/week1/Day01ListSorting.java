package codeborne.week1;


import java.util.ArrayList;
import java.util.List;

public class Day01ListSorting {

    public static List<Integer> sortingNumbersBubble(List<Integer> input) {
        long startTime = System.currentTimeMillis();
        boolean swap;
        List<Integer> numbersList = new ArrayList<>(input);
        for (int i = 0; i < numbersList.size() - 1; i++) {
            swap = false;
            for (int j = i + 1; j < numbersList.size(); j++) {
                if (numbersList.get(i) > numbersList.get(j)) {
                    int objectInMemory = numbersList.get(i);
                    numbersList.set(i, numbersList.get(j));
                    numbersList.set(j, objectInMemory);
                    swap = true;
                }
            }
            if(!swap){
                break;
            }
        }
        long endTime = System.currentTimeMillis();
        double usedTime = (endTime - startTime) / 1000.0;
        System.out.println(usedTime);

        return numbersList;
    }

    public static List<Integer> sortingNumbers(List<Integer> input) {
        long startTime = System.currentTimeMillis();

        for (int i = 0; i < input.size() - 1; i++) {
                if (input.get(i) > input.get(i + 1)) {
                    int objectInMemory = input.get(i);
                    input.set(i, input.get(i+1));
                    input.set(i + 1, objectInMemory);
                    i = -1;
            }
        }
        long endTime = System.currentTimeMillis();
        double usedTime = (endTime - startTime) / 1000.0;
        System.out.println(usedTime);

        return input;
    }
}