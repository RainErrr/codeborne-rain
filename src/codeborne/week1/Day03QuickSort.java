package codeborne.week1;

import java.io.IOException;
import java.util.List;

public class Day03QuickSort {

    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis();
        List<Integer> x = Day03NumbersReader.fileReaderListInt("1000000.txt");
        int low = 0;
        int high = x.size() - 1;

        quickSort(x, low, high);
        System.out.println(x);
        long endTime = System.currentTimeMillis();
        double usedTime = (endTime - startTime) / 1000.0;
        System.out.println(usedTime);
    }

    public static void quickSort(List<Integer> input, int low, int high) {
        if (input == null || input.size() == 0)
            return;

        if (low >= high)
            return;

        // pick the pivot
        int middle = low + (high - low) / 2;
        int pivot = input.get(middle);

        // make left < pivot and right > pivot
        int i = low, j = high;
        while (i <= j) {
            while (input.get(i) < pivot) {
                i++;
            }

            while (input.get(j) > pivot) {
                j--;
            }

            if (i <= j) {
                int temp = input.get(i);
                input.set(i, input.get(j));
                input.set(j, temp);
                i++;
                j--;
            }
        }

        // recursively sort two sub parts
        if (low < j)
            quickSort(input, low, j);

        if (high > i)
            quickSort(input, i, high);
    }
}