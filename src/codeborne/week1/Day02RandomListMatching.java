package codeborne.week1;

import java.util.ArrayList;
import java.util.List;

public class Day02RandomListMatching {

    public static List<Integer> listItemMatching(List<Integer> a, List<Integer> b) {
        List<Integer> list1 = new ArrayList<>();

        if (a.size() == 0 || b.size() == 0) {
            return list1;
        }
            for (Integer integer : a) {
                for (Integer value : b) {
                    if (integer.equals(value) && !list1.contains(integer)) {
                        list1.add(integer);
                    }
                }
            }
            return list1;
    }
}
