package codeborne.week1;

public class Day02MultiplicationTable {

    public static String multiplicationTable(int x, int y) {
        int maxMultiplicationResult = (int)(Math.log10(x*y)+1);
        if(x < 1 || y < 1) {
            return "Both values need to be bigger than 0";
        }
        StringBuilder table = new StringBuilder();
        table.append(" ".repeat(maxMultiplicationResult-1)).append("* |");
        for (int i = 1; i <= x; i++) {
            table.append(spacing(i, maxMultiplicationResult)).append(" ");
        }
        table.append("\n").append("-".repeat(maxMultiplicationResult)).append("--")
                .append("-".repeat(maxMultiplicationResult)).append("-".repeat((maxMultiplicationResult+1)*(x-1)))
                    .append("\n");

        for (int i = 1; i <= y; i++) {
            table.append(spacing(i, maxMultiplicationResult)).append(" |");
            for (int j = 1; j <= x; j++) {
               table.append(spacing(i*j, maxMultiplicationResult)).append(" ");
            }
            table.append("\n");
        }
        return table.toString();
    }
    public static StringBuilder spacing(int x, int maxMultiplicationResult) {
        StringBuilder s = new StringBuilder();
        if (x < 1) return s;
        if (x <= 9) s.append(" ".repeat(maxMultiplicationResult - 1)).append(x);
        else if (x <= 99) s.append(" ".repeat(maxMultiplicationResult - 2)).append(x);
        else if (x <= 999) s.append(" ".repeat(maxMultiplicationResult - 3)).append(x);
        else if (x <= 9999) s.append(" ".repeat(maxMultiplicationResult - 4)).append(x);
        else s.append(" ".repeat(maxMultiplicationResult - 5)).append(x);
        return s;
    }
}
