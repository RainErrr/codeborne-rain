package codeborne.week2;

public class Gate {

    enum Status {
        CLOSED,
        OPEN,
        CLOSING,
        OPENING,
        STOPPED,
        ERROR
    }

    private boolean isRestartNeeded;
    private boolean isBlocked;
    private Status gateStatus;
    private boolean isBlinking;

    public Gate(boolean isRestartNeeded, boolean isBlocked, Status gateStatus, boolean isBlinking) {
        this.isRestartNeeded = isRestartNeeded;
        this.isBlocked = isBlocked;
        this.gateStatus = gateStatus;
        this.isBlinking = isBlinking;
    }

    public boolean isRestartNeeded() {
        return isRestartNeeded;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public Status getGateStatus() {
        return gateStatus;
    }

    public boolean isBlinking() {
        return isBlinking;
    }

    @Override
    public String toString() {
        return "Gate:" +
                "\n\tIs restart needed: " + isRestartNeeded +
                "\n\tIs the gate blocked: " + isBlocked +
                "\n\tGate state: " + gateStatus +
                "\n\tIs signal light blinking: " + isBlinking
                ;
    }
}
