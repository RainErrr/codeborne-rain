package codeborne.week2;

public class Day11GateRemote {

    public static void main(String[] args) {
        Day11GateRemote signal = new Day11GateRemote();
        System.out.println(signal.signalInterpretation((byte) 1));
    }

    public Gate signalInterpretation(byte inputSignal) {
        if (inputSignal == 0b00000000) {
            System.out.println("Error in the system!");
            return new Gate(false, false, Gate.Status.ERROR, false);
        }
        return new Gate(
                isRestartNeeded(inputSignal),
                isBlocked(inputSignal),
                gateState(inputSignal),
                isBlinking(inputSignal)
        );
    }

    private boolean isRestartNeeded(byte input) {
        return (input & 128) == 128;
    }

    private boolean isBlocked(byte input) {
        return (input & 64) == 64;
    }

    private Gate.Status gateState(byte input) {
        if ((input & 2) == 2) return Gate.Status.CLOSED;
        if ((input & 4) == 4) return Gate.Status.OPEN;
        if ((input & 8) == 8) return Gate.Status.CLOSING;
        if ((input & 16) == 16) return Gate.Status.OPENING;
        if ((input & 32) == 32) return Gate.Status.STOPPED;
        else return Gate.Status.ERROR;
    }

    private boolean isBlinking(byte input) {
        return (input & 1) == 1;
    }
}
