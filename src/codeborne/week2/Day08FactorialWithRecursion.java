package codeborne.week2;


public class Day08FactorialWithRecursion {
    public static void main(String[] args) {
        int input = 5;
        long factorial = factorialWithTailRecursion(input);
        System.out.println(factorial);
    }


    public static long factorialWithRecursion(int input, int tail) {
        if (input >= 1) {
            return factorialWithRecursion(input - 1, input * tail);
        }
        return tail;
    }
    public static long factorialWithTailRecursion(int input) {

        return factorialWithRecursion(input, 1);
    }
}
