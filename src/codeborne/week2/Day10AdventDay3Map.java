package codeborne.week2;

import codeborne.week1.Day03NumbersReader;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day10AdventDay3Map {

    public static void main(String[] args) throws IOException {
        Day10AdventDay3Map advent = new Day10AdventDay3Map();

        System.out.println(advent.shortestDistance(
                advent.wireToMap(Day03NumbersReader.fileReaderListStr("advent03first.txt")),
                advent.wireToMap(Day03NumbersReader.fileReaderListStr("advent03Second.txt")))
        );
        System.out.println(advent.shortestWireLength(
                advent.wireToMap(Day03NumbersReader.fileReaderListStr("advent03first.txt")),
                advent.wireToMap(Day03NumbersReader.fileReaderListStr("advent03Second.txt")))
        );
    }

    private Map<String, Integer> wireToMap(List<String> firstList) {
        Map<String, Integer> firstWire = new HashMap<>();
        int x = 0, y = 0, mapCounter = 0;

        for (String s : firstList) {
            int[] pointer = rightDownLeftUpPointer(String.valueOf(s.charAt(0)));
            int numberForLength = Integer.parseInt(s.substring(1));
            for (int i = 0; i < numberForLength; i++) {
                int xPointer = x + pointer[0];
                int yPointer = y + pointer[1];
                mapCounter++;
                firstWire.put(xPointer + "," + yPointer, mapCounter);
                x = xPointer;
                y = yPointer;
            }
        }
        return firstWire;
    }

    public int shortestDistance(Map<String, Integer> firstWire, Map<String, Integer> secondWire) {
        int distance = 0;
        try {
            for (String key : firstWire.keySet()) {
                if (secondWire.containsKey(key)) {
                    String[] splitKey = key.split(",");
                    int tempValue = Math.abs(Integer.parseInt(splitKey[0])) + Math.abs(Integer.parseInt(splitKey[1]));
                    if (tempValue < distance || distance == 0) {
                        distance = tempValue;
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 1;
        } catch (NumberFormatException e) {
            return 2;
        }
        return distance;
    }

    public int shortestWireLength(Map<String, Integer> firstWire, Map<String, Integer> secondWire) {
        int sumOfSteps = 0;
        for (String key : firstWire.keySet()) {
            if (secondWire.containsKey(key)) {
                int tempValue = firstWire.get(key) + secondWire.get(key);
                System.out.println(tempValue);
                if (tempValue < sumOfSteps || sumOfSteps == 0) {
                    sumOfSteps = tempValue;
                }
            }
        }
        return sumOfSteps;
    }

    private int[] rightDownLeftUpPointer(String inputLetter) {
        int[] pointer;
        switch (inputLetter) {
            case "R":
                pointer = new int[]{1, 0};
                break;
            case "D":
                pointer = new int[]{0, -1};
                break;
            case "L":
                pointer = new int[]{-1, 0};
                break;
            case "U":
                pointer = new int[]{0, 1};
                break;
            default:
                return new int[]{0, 0, 7};
        }
        return pointer;
    }
}
