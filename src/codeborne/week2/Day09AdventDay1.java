package codeborne.week2;

import codeborne.week1.Day03NumbersReader;

import java.io.IOException;
import java.util.List;

public class Day09AdventDay1 {
    public static void main(String[] args) throws IOException {

        Day09AdventDay1 advent = new Day09AdventDay1();
        System.out.println(advent.fuelCalculation(Day03NumbersReader.fileReaderListInt("advent0101.txt")));
    }
    public long fuelCalculation(List<Integer> moduleMasses) {
        long sumOfFuel = 0;
        for(Integer moduleMass : moduleMasses) {
            sumOfFuel += moduleMass/3-2;
            if (sumOfFuel < 0) {
                return 0;
            }
            List<Integer> deepList = List.of(moduleMass/3-2);
            sumOfFuel += fuelCalculation(deepList);
            }
        return sumOfFuel;
    }
}

