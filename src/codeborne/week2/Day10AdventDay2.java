package codeborne.week2;

import codeborne.week1.Day03NumbersReader;

import java.io.IOException;
import java.util.List;

public class Day10AdventDay2 {
    public static void main(String[] args) throws IOException {
        Day10AdventDay2 advent = new Day10AdventDay2();
        System.out.println(advent.intCodeProgramMain(Day03NumbersReader.fileReaderListInt("advent0201.txt"), 12, 2));
        System.out.println(advent.findSpecialCase(19690720));
    }

    public int intCodeProgramMain(List<Integer> program, int valueProgramListOne, int valueProgramLisTwo) {
        try {
            program.set(1, valueProgramListOne);
            program.set(2, valueProgramLisTwo);
            for (int i = 0; i < program.size(); i = i + 4) {
                int firstVariable = program.get(program.get(i + 1));
                int secondVariable = program.get(program.get(i + 2));
                if (program.get(i) == 99) {
                    return program.get(0);
                } else if (program.get(i) == 1) {
                    program.set(program.get(i + 3), firstVariable + secondVariable);
                } else if (program.get(i) == 2) {
                    program.set(program.get(i + 3), firstVariable * secondVariable);
                } else {
                    System.out.println("Error" + i + program.get(i));
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        } catch (UnsupportedOperationException e) {
            return 1;
        }
        return program.get(0);
    }

    public int findSpecialCase(int numberOnZeroPosition) throws IOException {
        int specialCaseOut = 0;
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                if (intCodeProgramMain(Day03NumbersReader.fileReaderListInt("advent0201.txt"), i, j) == numberOnZeroPosition) {
                    if (i * 100 + j < specialCaseOut || specialCaseOut == 0) {
                        specialCaseOut = i * 100 + j;
                    }

                }
            }
        }
        return specialCaseOut;
    }
}