package codeborne.BasicWebServer;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Path;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ResponseHandlerTest {
    RequestHandler request = new RequestHandler();
    ResponseHandler response = new ResponseHandler();

    @Test
    void testIfFinalResponseWorksWithGET() throws IOException {
        InputStream input = new ByteArrayInputStream(("GET /index.html HTTP/1.1\r\nConnection: keep-alive\r\n\r\n").getBytes());
        Request testRequest = request.requestInObject(input);
        Request testRequest2 = new Request("GET", "/index.html", "HTTP/1.1", false, 0,
                Map.of("Connection:", "keep-alive") ,new byte[0]);
        byte[] expected = new GetResponse(response.responseStatusText(testRequest2),
                response.responseContentType(testRequest2),
                response.body(testRequest2)).responseAsByteArray();
        assertArrayEquals(expected, response.finalResponse(testRequest));
    }

    @Test
    void testIfResponseHandlerWorksWithPOST() throws IOException {
        InputStream input = new ByteArrayInputStream(("POST /index.html HTTP/1.1\r\nConnection: keep-alive\r\n\r\n").getBytes());
        Request testRequest = request.requestInObject(input);
        Request testRequest2 = new Request("GET", "/index.html", "HTTP/1.1", false, 0,
                Map.of("Connection:", "keep-alive") ,new byte[0]);
        byte[] expected = new PostResponse(response.responseStatusText(testRequest2),
                response.responseContentType(testRequest2),
                response.body(testRequest2)).responseAsByteArray();
        assertArrayEquals(expected, response.finalResponse(testRequest));
    }
//
    @Test
    void testIfResponseHandlerWorksWithHEAD() throws IOException {
        InputStream input = new ByteArrayInputStream(("HEAD /index.html HTTP/1.1\r\nConnection: keep-alive\r\n\r\n").getBytes());
        Request testRequest = request.requestInObject(input);
        Request testRequest2 = new Request("GET", "/index.html", "HTTP/1.1", false, 0,
                Map.of("Connection:", "keep-alive") ,new byte[0]);
        byte[] expected = new HeadResponse(response.responseStatusText(testRequest2),
                response.responseContentType(testRequest2),
                response.body(testRequest2)).responseAsByteArray();
        assertArrayEquals(expected, response.finalResponse(testRequest));
    }

    @Test
    void testIfResponseHandlerWorksWith501() throws IOException {
        InputStream input = new ByteArrayInputStream(("PUT /index.html HTTP/1.1\r\nConnection: keep-alive\r\n\r\n").getBytes());
        Request testRequest = request.requestInObject(input);
        byte[] expected = new NotImplemented501Response("501 Not implemented",
                "text/html",
                response.readFileForResponse(new File("Resources", "501.html"))).responseAsByteArray();
        assertArrayEquals(expected, response.finalResponse(testRequest));
    }

    @Test
    void testResponseHandlerWhenInputIsEmpty() throws IOException {
        Request testRequest = new Request(
                "", "", "",
                false, 0, Map.of(), new byte[0]);
        byte[] expected = new GetResponse("501 Not implemented",
                "text/html",
                response.readFileForResponse(new File("Resources", "501.html"))).responseAsByteArray();
        assertArrayEquals(expected, response.finalResponse(testRequest));
    }

    @Test
    void testIfParsedFileNameWorks() {
    assertEquals("index.html", response.parsedFileName("/index.html"));
}

    @Test
    void testParsedFileNameWhenInputIsASingleFwdSlash() {
        assertEquals("index.html", response.parsedFileName("/"));
    }

    @Test
    void testParsedFileNameWhenInputIsEmpty() {
        assertEquals("index.html", response.parsedFileName(""));
    }

    @Test
    void testIfTurnFileNameToFilePathWorks() {
        Path expectedPath = Path.of("Resources", "index.html");
        assertEquals(expectedPath, response.turnFileNameToFilePath("index.html"));
    }

    @Test
    void testTurnFileNameToFilePathWhenInputIsEmpty() {
        assertEquals(Path.of("Resources", "index.html"), response.turnFileNameToFilePath(""));
    }

    @Test
    void testIfFilePathFixForEdgeCasesWorks() {
        Path path = Path.of("Resources", "index.html");
        assertEquals("index.html", response.filePathFixForEdgeCases(path));
    }

    @Test
    void testFilePathFixForEdgeCasesWhenInputIsEmpty() {
        assertEquals("404.html", response.filePathFixForEdgeCases(Path.of("")));
    }

    @Test
    void testFilePathFixForEdgeCasesWhenInputFileDoesNotExist() {
        assertEquals("404.html", response.filePathFixForEdgeCases(Path.of("Resources", "index.ht")));
    }

    @Test
    void testIfCreateResponseFileWorks() {
        assertEquals(new File("Resources", "index.html"), response.createResponseFile("index.html"));
    }

    @Test
    void testIfReadFileForResponseWorks() throws IOException {
        File file = new File("Resources", "index.html");
        byte[] expectedArray;
        FileInputStream inputStream = new FileInputStream(file);
        expectedArray = inputStream.readAllBytes();
        assertArrayEquals(expectedArray, response.readFileForResponse(file));
    }

    @Test
    void testReadFileForResponseWhenFileDoesNotExist() throws IOException {
        File file = new File("Resources", "404.html");
        byte[] expectedArray;
        FileInputStream inputStream = new FileInputStream(file);
        expectedArray = inputStream.readAllBytes();
        assertArrayEquals(expectedArray, response.readFileForResponse(new File("")));
    }

    @Test
    void testIfStatusTextWorks() {
        File file = new File("Resources", "index.html");
        assertEquals("200 OK", response.statusText(file));
    }

    @Test
    void testStatusTextWhenInputFileDoesNotExist() {
        File file = new File("Resources", "404.html");
        assertEquals("404 File Not Found", response.statusText(file));
    }

    @Test
    void testStatusTextWhenInputFileMethodTypeWasNotImplemented() {
        File file = new File("Resources", "index.htm");
        assertEquals("501 Not Implemented", response.statusText(file));
    }

    @Test
    void testStatusTextWhenInputIsEmpty() {
        File file = new File("");
        assertEquals("501 Not Implemented", response.statusText(file));
    }

    @Test
    void testIfContentTypeWorks() {
        assertEquals("text/html", response.contentType("index.html"));
        assertEquals("image/icon", response.contentType("something.ico"));
        assertEquals("image/png", response.contentType("something.png"));
        assertEquals("image/jpeg", response.contentType("something.jpg"));
        assertEquals("text/plain", response.contentType("something.txt"));
    }

    @Test
    void testContentTypeWhenInputIsEmpty() {
        assertEquals("text/plain", response.contentType(""));
    }
}