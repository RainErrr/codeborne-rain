package codeborne.BasicWebServer;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

class RequestHandlerTest {
    RequestHandler request = new RequestHandler();

    @Test
    void testIfInitialReadFromStreamToStringWorks() throws IOException {
        InputStream input = new ByteArrayInputStream(("GET /index.html HTTP/1.1\r\nConnection: keep-alive\r\n\r\n").getBytes());
        assertEquals("GET /index.html HTTP/1.1\r\nConnection: keep-alive\r\n",
                request.requestHeaderFromStream(input));
    }

    @Test
    void testIfInitialReadFromStreamBreaksFromBlankLine() throws IOException {
        InputStream input = new ByteArrayInputStream(("GET /index.html HTTP/1.1\r\nConnection: keep-alive\r\n\r\nLast Message").getBytes());
        assertEquals("GET /index.html HTTP/1.1\r\nConnection: keep-alive\r\n", request.requestHeaderFromStream(input));
    }

    @Test
    void testInitialReadFromStreamWhenInputIsEmpty() throws IOException {
        InputStream input = new ByteArrayInputStream(("").getBytes());
        assertEquals("", request.requestHeaderFromStream(input));
    }

    @Test
    void testInitialReadFromStreamThatLineBreaksAreWithBackSlashesAndInWindowsFormat() throws IOException {
        InputStream inputStream = new ByteArrayInputStream(("first\r\nsecond\r\n").getBytes());
        assertEquals("first\r\nsecond\r\n", request.requestHeaderFromStream(inputStream));
    }

    @Test
    void testInitialReadFromStreamWhenThereIsNoEmptyLineForHeaderEnd() throws IOException {
        InputStream input = new ByteArrayInputStream(("GET /index.html HTTP/1.1\r\n").getBytes());
        assertEquals("GET /index.html HTTP/1.1\r\n", request.requestHeaderFromStream(input));
    }

    @Test
    void testSplitRequestHeaderToStringWorks() {
        String input = "first\r\nsecond\r\nthird";
        assertArrayEquals(new String[]{"first", "second", "third"}, request.splitRequestHeader(input));
    }

    @Test
    void testSplitRequestHeaderWhenInputIsEmpty() {
        assertArrayEquals(new String[]{""}, request.splitRequestHeader(""));
    }

    @Test
    void testFindIfRequestHasBodyWorks() {
        String[] inputTrue = new String[]{"first", "Content-Length: "};
        String[] inputFalse = new String[]{"first"};
        assertTrue(request.findIfRequestHasBody(inputTrue));
        assertFalse(request.findIfRequestHasBody(inputFalse));
    }

    @Test
    void testFindIfRequestHasBodyWhenInputIsPartial() {
        String[] input = new String[]{"Content-Length"};
        assertFalse(request.findIfRequestHasBody(input));
    }

    @Test
    void testFindIfRequestHasBodyWhenInputIsEmpty() {
        assertFalse(request.findIfRequestHasBody(new String[]{}));
    }

    @Test
    void testIfParseOutLineWithLengthOfBodyWorks() {
        String[] input = new String[]{"first", "Content-Length: 200"};
        assertEquals("Content-Length: 200", request.parseOutLineWithLengthOfBody(input));
    }

    @Test
    void testParseOutLineWithLengthOfBodyWhenInputIsEmpty() {
        assertEquals("", request.parseOutLineWithLengthOfBody(new String[]{}));
    }

    @Test
    void testParseOutLineWithLengthOfBodyWhenNoMatchInInput() {
        String[] input = new String[]{"first", "second", "third"};
        assertEquals("", request.parseOutLineWithLengthOfBody(input));
    }

    @Test
    void testIfLengthOfBodyWorks() {
        assertEquals(200, request.lengthOfBody("Content-Length: 200"));
    }

    @Test
    void testLengthOfBodyWhenInputWasPartiallyReceived() {
        assertEquals(0, request.lengthOfBody("Content-Length:  "));
    }

    @Test
    void testLengthOfBodyWhenInputIsEmpty() {
        assertEquals(0, request.lengthOfBody(""));
    }

    @Test
    void testIfRequestInObjectWorks() throws IOException {
        InputStream input = new ByteArrayInputStream(("GET /index.html HTTP/1.1\r\nContent-Length: 5\r\n\r\n").getBytes());
        Request testRequest = request.requestInObject(input);
        assertEquals("GET", testRequest.getMethodType());
        assertEquals("/index.html", testRequest.getFilePath());
        assertTrue(testRequest.isDoesBodyExist());
        assertEquals(5, testRequest.getBodyLength());
        assertArrayEquals(new byte[0], testRequest.getBody());
    }

    @Test
    void testIfReturnRequestWithUpdatedBodyWorks() throws IOException {
        InputStream testInPut = new ByteArrayInputStream(("rain7").getBytes());
        InputStream input = new ByteArrayInputStream(("GET /index.html HTTP/1.1\r\nContent-Length: 5\r\n\r\n").getBytes());
        Request testRequest = request.requestInObject(input);
        assertArrayEquals("rain7".getBytes(), request.returnRequestWithUpdatedBody(testInPut, testRequest).getBody());
    }

    @Test
    void testReturnRequestWithUpdatedBodyWhenNoBody() throws IOException {
        InputStream testInput = new ByteArrayInputStream(("").getBytes());
        InputStream input = new ByteArrayInputStream(("GET /index.html HTTP/1.1\r\nContent-Length: 5\r\n\r\n").getBytes());
        Request testRequest = request.requestInObject(input);
        assertArrayEquals(new byte[0], request.returnRequestWithUpdatedBody(testInput, testRequest).getBody());
    }

    @Test
    void testReturnRequestWithUpdatedBodyWithEmptyRequest() throws IOException {
        InputStream testInput = new ByteArrayInputStream(("").getBytes());
        InputStream input = new ByteArrayInputStream(("").getBytes());
        Request testRequest = request.requestInObject(input);
        assertArrayEquals(new byte[0], request.returnRequestWithUpdatedBody(testInput, testRequest).getBody());
    }

    @Test
    void testIfHeaderStatusLineWorks() {
        String[] input = new String[]{"GET /index.html HTTP/1.1", "plaplapla"};
        assertEquals("GET /index.html HTTP/1.1", request.headerStatusLine(input));
    }

    @Test
    void testHeaderStatusLineWhenInputIsEmpty() {
        assertEquals("", request.headerStatusLine(new String[]{}));
    }

    @Test
    void testIfSplitStatusLineWorks() {
        String[] testOut = new String[]{"GET", "/index.html", "HTTP/1.1"};
        assertArrayEquals(testOut, request.splitStatusLine("GET /index.html HTTP/1.1"));
    }

    @Test
    void testSplitStatusLineWhenInputIsEmpty() {
        assertArrayEquals(new String[]{}, request.splitStatusLine(""));
    }

    @Test
    void testSplitStatusLineWhenInputIsSingleString() {
        String[] testOut = new String[]{"hmm"};
        assertArrayEquals(testOut, request.splitStatusLine("hmm"));
    }

    @Test
    void testSplitStatusLineWhenMoreThanOneSpaceInARow() {
        String[] testOut = new String[]{"one", "two"};
        assertArrayEquals(testOut, request.splitStatusLine("one  two"));
    }

    @Test
    void testIfRequestMethodWorks() {
        String[] input = new String[]{"one", "two"};
        assertEquals("one", request.requestMethod(input));
    }

    @Test
    void testRequestMethodWhenInputIsEmpty() {
        assertEquals("", request.requestMethod(new String[]{}));
    }

    @Test
    void testIfRequestedFileWorks() {
        String[] input = new String[]{"one", "two"};
        assertEquals("two", request.requestedFile(input));
    }

    @Test
    void testRequestedFileWhenInputIsEmpty() {
        assertEquals("", request.requestedFile(new String[]{}));
    }
}
