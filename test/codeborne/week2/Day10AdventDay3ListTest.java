package codeborne.week2;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day10AdventDay3ListTest {
    Day10AdventDay3List advent = new Day10AdventDay3List();


    @Test
    void testDistance() {
        assertEquals(3, advent.distanceList(List.of("1, 0", "2, 0", "2, 1", "2, 2", "2, 3",
                "1, 3", "0, 3", "-1, 3", "-1, 2", "-2, 2"),
                List.of("0, -1", "0, -2", "-1, -2", "-1, -1", "-1, 0",
                        "-1, 1", "-1, 2", "-1, 3", "-1, 4", "-1, 5")));
    }
    @Test
    void testDistanceWithZeroInput() {
        assertEquals(1, advent.distanceList(List.of("0"), List.of("0")));
    }
    @Test
    void testDistanceWithWrongInput() {
        assertEquals(0, advent.distanceList(List.of("20"), List.of("30")));
    }
    @Test
    void testDistanceWithEmptyMap() {
        assertEquals(0, advent.distanceList(List.of(), List.of()));
    }
    @Test
    void testDistanceWithEmptyString() {
        assertEquals(2, advent.distanceList(List.of(""), List.of("")));
    }

    @Test
    void testLength() {
        assertEquals(16, advent.lengthList(List.of("1, 0", "2, 0", "2, 1", "2, 2", "2, 3",
                "1, 3", "0, 3", "-1, 3", "-1, 2", "-2, 2"),
                List.of("0, -1", "0, -2", "-1, -2", "-1, -1", "-1, 0",
                        "-1, 1", "-1, 2", "-1, 3", "-1, 4", "-1, 5")));
    }
    @Test
    void testLengthWithZeroInput() {
        assertEquals(2, advent.lengthList(List.of("0"), List.of("0")));
    }
    @Test
    void testLengthWithWrongInput() {
        assertEquals(0, advent.lengthList(List.of("45"), List.of("90")));
    }
    @Test
    void testLengthWithEmptyMap() {
        assertEquals(0, advent.lengthList(List.of(), List.of()));
    }
    @Test
    void testLengthWithEmptyString() {
        assertEquals(2, advent.lengthList(List.of(""), List.of("")));
    }
}