package codeborne.week2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day08FactorialWithRecursionTest {

    @Test
void testWehnInputIsZero() {
    assertEquals(1, Day08FactorialWithRecursion.factorialWithTailRecursion(0));
}
    @Test
    void testWhenInputIsOne() {
        assertEquals(1, Day08FactorialWithRecursion.factorialWithTailRecursion(1));
    }
    @Test
    void testWhenInputIsNegative() {
        assertEquals(1, Day08FactorialWithRecursion.factorialWithTailRecursion(-1));
    }
    @Test
    void testIfFactorialIsCorrect() {
        assertEquals(24, Day08FactorialWithRecursion.factorialWithTailRecursion(4));
    }
}