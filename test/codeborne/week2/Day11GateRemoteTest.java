package codeborne.week2;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day11GateRemoteTest {
    Day11GateRemote remote = new Day11GateRemote();

    @Test
    void testWhenInputNegative() {
       assertTrue(remote.signalInterpretation((byte)-1).isRestartNeeded());
       assertTrue(remote.signalInterpretation((byte)-1).isBlocked());
       assertEquals(Gate.Status.CLOSED, remote.signalInterpretation((byte)-1).getGateStatus());
       assertTrue(remote.signalInterpretation((byte)-1).isBlinking());
    }
    @Test
    void testWhenInputZero() {
        assertFalse(remote.signalInterpretation((byte)0).isRestartNeeded());
        assertFalse(remote.signalInterpretation((byte)0).isBlocked());
        assertEquals(Gate.Status.ERROR, remote.signalInterpretation((byte)0).getGateStatus());
        assertFalse(remote.signalInterpretation((byte)0).isBlinking());
    }
    @Test
    void testWhenInputOne() {
        assertFalse(remote.signalInterpretation((byte)1).isRestartNeeded());
        assertFalse(remote.signalInterpretation((byte)1).isBlocked());
        assertEquals(Gate.Status.ERROR, remote.signalInterpretation((byte)1).getGateStatus());
        assertTrue(remote.signalInterpretation((byte)1).isBlinking());
    }
    @Test
    void testWhenInputTwo() {
        assertFalse(remote.signalInterpretation((byte)2).isRestartNeeded());
        assertFalse(remote.signalInterpretation((byte)2).isBlocked());
        assertEquals(Gate.Status.CLOSED, remote.signalInterpretation((byte)2).getGateStatus());
        assertFalse(remote.signalInterpretation((byte)2).isBlinking());
    }
    @Test
    void testWhenInputFour() {
        assertFalse(remote.signalInterpretation((byte)4).isRestartNeeded());
        assertFalse(remote.signalInterpretation((byte)4).isBlocked());
        assertEquals(Gate.Status.OPEN, remote.signalInterpretation((byte)4).getGateStatus());
        assertFalse(remote.signalInterpretation((byte)4).isBlinking());
    }
    @Test
    void testWhenInputEight() {
        assertFalse(remote.signalInterpretation((byte)8).isRestartNeeded());
        assertFalse(remote.signalInterpretation((byte)8).isBlocked());
        assertEquals(Gate.Status.CLOSING, remote.signalInterpretation((byte)8).getGateStatus());
        assertFalse(remote.signalInterpretation((byte)8).isBlinking());
    }
    @Test
    void testWhenInput16() {
        assertFalse(remote.signalInterpretation((byte)16).isRestartNeeded());
        assertFalse(remote.signalInterpretation((byte)16).isBlocked());
        assertEquals(Gate.Status.OPENING, remote.signalInterpretation((byte)16).getGateStatus());
        assertFalse(remote.signalInterpretation((byte)16).isBlinking());
    }
    @Test
    void testWhenInput32() {
        assertFalse(remote.signalInterpretation((byte)32).isRestartNeeded());
        assertFalse(remote.signalInterpretation((byte)32).isBlocked());
        assertEquals(Gate.Status.STOPPED, remote.signalInterpretation((byte)32).getGateStatus());
        assertFalse(remote.signalInterpretation((byte)32).isBlinking());
    }
    @Test
    void testWhenInput64() {
        assertFalse(remote.signalInterpretation((byte)64).isRestartNeeded());
        assertTrue(remote.signalInterpretation((byte)64).isBlocked());
        assertEquals(Gate.Status.ERROR, remote.signalInterpretation((byte)64).getGateStatus());
        assertFalse(remote.signalInterpretation((byte)64).isBlinking());
    }
    @Test
    void testWhenInput128() {
        assertTrue(remote.signalInterpretation((byte)128).isRestartNeeded());
        assertFalse(remote.signalInterpretation((byte)128).isBlocked());
        assertEquals(Gate.Status.ERROR, remote.signalInterpretation((byte)128).getGateStatus());
        assertFalse(remote.signalInterpretation((byte)128).isBlinking());
    }
    @Test
    void testIfYouGetErrorInGateStatus() {
        assertFalse(remote.signalInterpretation((byte)10).isRestartNeeded());
        assertFalse(remote.signalInterpretation((byte)10).isBlocked());
        assertEquals(Gate.Status.CLOSED, remote.signalInterpretation((byte)10).getGateStatus());
        assertFalse(remote.signalInterpretation((byte)10).isBlinking());
    }
    @Test
    void testIfTwoBooleansCanBeTrue() {
        assertFalse(remote.signalInterpretation((byte)65).isRestartNeeded());
        assertTrue(remote.signalInterpretation((byte)65).isBlocked());
        assertEquals(Gate.Status.ERROR, remote.signalInterpretation((byte)65).getGateStatus());
        assertTrue(remote.signalInterpretation((byte)65).isBlinking());
    }
    @Test
    void testWhenAllExceptfistBitAreUp() {
        assertFalse(remote.signalInterpretation((byte)127).isRestartNeeded());
        assertTrue(remote.signalInterpretation((byte)127).isBlocked());
        assertEquals(Gate.Status.CLOSED, remote.signalInterpretation((byte)127).getGateStatus());
        assertTrue(remote.signalInterpretation((byte)127).isBlinking());
    }
    @Test
    void test() {
        assertFalse(remote.signalInterpretation((byte)0b01100001).isRestartNeeded());
        assertTrue(remote.signalInterpretation((byte)0b01100001).isBlocked());
        assertEquals(Gate.Status.STOPPED, remote.signalInterpretation((byte)0b01100001).getGateStatus());
        assertTrue(remote.signalInterpretation((byte)0b01100001).isBlinking());
    }
}