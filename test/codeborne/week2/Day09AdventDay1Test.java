package codeborne.week2;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day09AdventDay1Test {
    Day09AdventDay1 advent = new Day09AdventDay1();
    @Test
    void testWhenInputIsZero() {
        assertEquals(0, advent.fuelCalculation(List.of(0)));
    }
    @Test
    void testWhenInputIsOne() {
        assertEquals(0, advent.fuelCalculation(List.of(1)));
    }
    @Test
    void testWhenInputIsNegative() {
        assertEquals(0, advent.fuelCalculation(List.of(-1)));
    }
    @Test
    void testWhenInputIsEmptyList() {
        assertEquals(0, advent.fuelCalculation(List.of()));
    }
    @Test
    void testIfFunctionWorks() {
        assertEquals(1, advent.fuelCalculation(List.of(9, 6)));
    }
}