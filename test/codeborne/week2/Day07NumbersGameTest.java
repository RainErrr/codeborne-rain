package codeborne.week2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day07NumbersGameTest {

    @Test
    void testGameLogicBoundsWithNegativeNumbers() {
        assertEquals("Guess must be between 1 -100- try again.",
                Day07NumbersGame.gameLogic("-1", 20));
    }
    @Test
    void testGameLogicBoundsWithOutOfBoundsNumber() {
        assertEquals("Guess must be between 1 -100- try again.",
                Day07NumbersGame.gameLogic("101", 50));
    }
    @Test
    void testGameLogicWhenInputBiggerThanGeneratedNumber() {
        assertEquals("The number is smaller.",
                Day07NumbersGame.gameLogic("50", 33));
    }
    @Test
    void testGameLogicWhenInputIsSmallerThanGeneratedNumber() {
        assertEquals("The number is bigger.", Day07NumbersGame.gameLogic("24", 47));
    }
    @Test
    void testGameLogicWhenInputEqualsGeneratedNumber() {
        assertEquals("Congrats you won!", Day07NumbersGame.gameLogic("7", 7));
    }
}