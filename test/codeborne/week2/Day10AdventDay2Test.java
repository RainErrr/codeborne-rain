package codeborne.week2;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day10AdventDay2Test {
    Day10AdventDay2 advent = new Day10AdventDay2();

    @Test
    void testIfMainOctOneIsWorking() {
        assertEquals(3, advent.intCodeProgramMain(Arrays.asList(1, 1, 2, 0, 99, 1, 1, 1, 1, 1, 1, 1, 1), 12, 2));
    }
    @Test
    void testIfMainOctTwoIsWorking() {
        assertEquals(2, advent.intCodeProgramMain(Arrays.asList(2, 1, 2, 0, 99, 1, 1, 1, 1, 1, 1, 1, 1), 12, 2));
    }
    @Test
    void testIfMainOct99IsWorking() {
        assertEquals(3, advent.intCodeProgramMain(Arrays.asList(1, 1, 2, 0, 99, 1, 1, 1, 1, 1, 1, 1, 1), 12, 2));
    }
    @Test
    void testIfMainErrorIsWorking() {
        assertEquals(20, advent.intCodeProgramMain(Arrays.asList(20, 1, 3, 0, 99, 1, 1, 1, 1, 1, 1, 1, 1), 12,2));
    }
    @Test
    void testIfMainInputIsEmptyList() {
        assertEquals(1, advent.intCodeProgramMain(Collections.emptyList(), 0, 0));
    }
    @Test
    void testIfSpecialCaseIsWorking() throws IOException {
        assertEquals(9507, advent.findSpecialCase(19690720));
    }
    @Test
    void testIfSpecialCaseReturnsZero() throws IOException {
        assertEquals(0, advent.findSpecialCase(1000000000));
    }
    @Test
    void testWhenSpecialCaseInputZero() throws IOException {
        assertEquals(0, advent.findSpecialCase(0));
    }
    @Test
    void testWhenSpecialCaseInputOne() throws IOException {
        assertEquals(0, advent.findSpecialCase(1));
    }
    @Test
    void testWhenSpecialCaseInputNegative() throws IOException {
        assertEquals(0, advent.findSpecialCase(-1));
    }
    @Test
    void testWhenFirstItemOFListIs99() {
        assertEquals(99, advent.intCodeProgramMain(Arrays.asList(99, 0, 1, 2), 2, 1));
    }
}