package codeborne.week3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day15AdventDay04Test {
    Day15AdventDay04 advent = new Day15AdventDay04();

    @Test
    void testWithNegativeInputRange() {
        assertEquals(0, advent.numberRange(-20, 0));
    }
    @Test
    void testWithZeroInput() {
        assertEquals(0, advent.numberRange(0,0));
    }
    @Test
    void testWithOutOfBoundsInput() {
        assertEquals(0, advent.numberRange(99998, 1000000));
    }
    @Test
    void testWhenStartBiggerThanEnd() {
        assertEquals(0, advent.numberRange(20, 10));
    }
    @Test
    void testIfWorksSimpleRange() {
        assertEquals(1, advent.numberRange(100000, 111122));
    }
}