package codeborne.week1;


import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class Day01FibonacciSequenceTest {

    @Test
    public void testIfSequenceIsTrue() {
        assertEquals(List.of(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144), Day01FibonacciSequence.fibonacciSequence(13));
    }
    @Test
    public void testIfNegative() {
        assertEquals(List.of(), Day01FibonacciSequence.fibonacciSequence(-1));
    }
    @Test
    public void testIfPosition12True() {
        assertEquals(Integer.valueOf(89), Day01FibonacciSequence.fibonacciSequence(12).get(11));
//        assertEquals(Arrays.toString(Day01FibonacciSequence.fibonacciSequence(12).toArray()).substring(38, 40), "89");
    }
}