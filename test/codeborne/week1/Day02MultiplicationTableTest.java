package codeborne.week1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day02MultiplicationTableTest {

    @Test
    void testIfInputZero() {
        assertEquals("Both values need to be bigger than 0", Day02MultiplicationTable.multiplicationTable(0, 1));
        assertEquals("Both values need to be bigger than 0", Day02MultiplicationTable.multiplicationTable(1, 0));
        assertEquals("Both values need to be bigger than 0", Day02MultiplicationTable.multiplicationTable(0, 0));
    }
    @Test
    void testIfInputIsNegative() {
        assertEquals("Both values need to be bigger than 0", Day02MultiplicationTable.multiplicationTable(-1, 2));
    }
    @Test
    void testIfStringLengthCorrect() {
        assertEquals(31, Day02MultiplicationTable.multiplicationTable(2, 2).length());
    }
    @Test
    void testIfMultiplyingWorks() {
        assertEquals("25", Day02MultiplicationTable.multiplicationTable(5, 5).substring(Day02MultiplicationTable.multiplicationTable(5, 5).length()-4, Day02MultiplicationTable.multiplicationTable(5, 5).length()-2));
    }
    @Test
    void testIfTableIsCorrect() {
        assertEquals(" * | 1  2  3  4  5 \n" +
                "------------------\n" +
                " 1 | 1  2  3  4  5 \n" +
                " 2 | 2  4  6  8 10 \n" +
                " 3 | 3  6  9 12 15 \n" +
                " 4 | 4  8 12 16 20 \n" +
                " 5 | 5 10 15 20 25 \n", Day02MultiplicationTable.multiplicationTable(5,5));
    }
//    @Test
//    void testIfSpacingFunctionWorks() {
//        assertEquals(new StringBuilder(" 8"),Day02MultiplicationTable.spacing(8, 2));
//    }
//    @Test
//    void testZeroValueInSpacing() {
//        assertEquals("", Day02MultiplicationTable.spacing(0));
//    }
//    @Test
//    void testWhenXValueIsBiggerThan10000() {
//        assertEquals("" + 10001, Day02MultiplicationTable.spacing(10001));
//    }
//    @Test
//    void testNegativeValueInSpacing() {
//        assertEquals("", Day02MultiplicationTable.spacing(-1));
//    }
}