package codeborne.week1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day02PalindromeTest {

    @Test
    void testIfTrue() {
        assertTrue(Day02Palindrome.checkIfStringIsPalindrome("Racecar"));
    }
    @Test
    void testIfFalse() {
        assertFalse(Day02Palindrome.checkIfStringIsPalindrome("Mina"));
    }
}