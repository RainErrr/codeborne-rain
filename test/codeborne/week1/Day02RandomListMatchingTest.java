package codeborne.week1;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day02RandomListMatchingTest {

    @Test
    void testIfMatchingWorks() {
        assertEquals(
                Day01ListSorting.sortingNumbers(List.of(1, 4, 76, 100, -33)),
                Day02RandomListMatching.listItemMatching(
                        Day01ListSorting.sortingNumbers(new ArrayList<>(List.of(20, 40, 1, 4, 76, 100, -33))),
                        Day01ListSorting.sortingNumbers(new ArrayList<>(List.of(1, 5, -33, 2, 5, 25, 100, 4, 76)))));
    }
    @Test
    void testZeroValueTables() {
        assertEquals(List.of(0), Day02RandomListMatching.listItemMatching(new ArrayList<>(List.of(0, 0, 0)), new ArrayList<>(List.of(0, 0, 0))));
    }
    @Test
    void testWithEmptyTables() {
        assertEquals(List.of(), Day02RandomListMatching.listItemMatching(List.of(), List.of()));
    }
    @Test
    void testWithNullVariable() {
        assertNull(Day02RandomListMatching.listItemMatching(null, null));
    }
}